#--------------------------------------------EXO 1-----------------------------------------------------------

def week():
    #Declaration du list week
    week= ["Monday","Tuesday","Wednesday","Thirsday","Friday","Saturday","Sunday"]

    #Récupérer seulement les 5 premiers jours de la semaine d’une part, 
    #et ceux du week-end d’autre part

    print("\nQuestion 1 :\n")

    #Premier method

    print("5 premiers jours de la semaine : ",week[0:5])
    print("Jours du week-end : ",week[5:])

    print("\nQuestion 2 :\n")

    #Second method

    print("5 premiers jours de la semaine (autre methode) : ",week[0:-2])
    print("Jours du week-end (autre methode) : ",week[5:])

    #Deux manières pour accéder au dernier jour de la semaine

    print("\nQuestion 3 :\n")
    print("Dernier jour de la semaine : ",week[6])
    print("Dernier jour de la semaine (autre methode): ",week[-1])

    #Inversez les jours de la semaine en une commande

    print("\nQuestion 4 :\n")
    week = week[::-1] #week[-1::-1]

    print("Jour de la semaine inversé : ",week)

#--------------------------------------------EXO 2-----------------------------------------------------------
def saisons():
    printemps = ["Mars","Avril","Mai"]
    ete = ["Juin","Juillet","Août"]
    automne = ["Septembre","Octobre","Novembre"]
    hiver = ["Décembre","Janvier","Février"]     
    saisons = [hiver,printemps,ete,automne]

    print("\nsaisons : [")
    for i in range (0,len(saisons),1):
        print (saisons[i],end=";")
    print("]")
    print("\n\n1. saisons[2] : ",saisons[2])
    print("2. saisons[1][0] : " ,saisons[1][0])
    print("3. saisons[1:2] : " ,saisons[1:2])
    print("4. saisons[:][1] : " ,saisons[:][1])

#--------------------------------------------EXO 3-----------------------------------------------------------
#Instructions range() et list().
def multiplication():
    print ("\nMultiplication par 9 en une seule commande : ",list(range(0,91,9)))

#--------------------------------------------EXO 4-----------------------------------------------------------
def nbPaire():
    print(f"\nNombre de nombre paire en une seule commande : {len(range(2, 10001,2))}")

#--------------------------------------------EXO 5(ListAndIndice)--------------------------------------------
def listeEtIndice():
    week = ["Monday","Tuesday","Wednesday","Thirsday","Friday","Saturday","Sunday"]
    print ("\nweek : ",week)
    print("week[4] : ",week[4])

    #Permutations
    temp = week[0]
    week[0] = week[-1]
    week[-1] = temp
    print ('-'*20+"Apres permutation de week[0] et week[6]"+'-'*12+"\nweek[6]*20 : ",week[6]*12)

#--------------------------------------------EXO 6(ListAndRange)----------------------------------------------
def listeEtRange():
    lstVide =[]              #Declaration d'une liste vide
    print(f"\nList vide : {lstVide}")
    lsFlottant = [0.0]*5     #Initialisation et declaration d'une liste flottante
    print(f"List flottante : {lsFlottant}")
    print('-'*50)
    print(f"Range(4) = {list(range(4))}")
    print(f"Range(4, 8) = {list(range(4, 8))}")
    print(f"Range(2, 9, 2) = {list(range(4, 9, 2))}")
    print('-'*50)

    #Fusion des listes

    lstElmnt = list(range(6)) + lstVide + lsFlottant  
    print(lstElmnt)

 





