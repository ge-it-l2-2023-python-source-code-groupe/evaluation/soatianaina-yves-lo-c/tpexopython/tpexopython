#----------------------------------5.4.1 Boucles de base------------------------------------------

#first method for loop "for"

# 5.4.1 Boucles de base

def boucles_de_base():
    liste = ['cow', 'mouse', 'yeast', 'bacteria']

    print(f"\nListe = {liste}")

    for value in liste:
        print(value)
        
    print("-" * 10)

    for i in range(4):
        print(liste[i])

    print("-" * 10)

    i = 0
    while i < 4:
        print(liste[i])
        i += 1

 
#-----------------------------------5.4.2 Boucle et jours de la semaine-----------------------------------------

def boucles_et_jours_de_la_semaine():
	week =["monday","Tuesday","Wednesday","thursday","Friday","Saturday","Sunday"]

	print("\nMonday -----> sunday\n")
		#first method
	for day in week:
		print(day)
	print("-"*20)

		#second method
	for i in range (7):
		print(week[i])
	print("-"*20)

	print("Sunday ---> monday\n")
		#third method
	for i in range (6,-1,-1):
		print(week[i])
	print("-"*20)

	i=0
	while i <7:
		if(i==5 or i==6):
			print(week[i])
		i=i+1
    
#--------------------------------------5.4.3 Nombres de 1 à 10 sur une ligne--------------------------------------

def nb_de1_10_sur_une_ligne():
	print("\n")
	for i in range (1,11,1):
		print(i , end="  ")
	print("\n")

#--------------------------------------5.4.4 Nombres pairs et impairs-------------------------------------

def nb_Pairs_et_Impairs():
	impair = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
	print("\n",impair)

	paire = [None] * 11

	for i in range (0,11,1):
		paire[i] = impair[i] + 1
	print (paire)

#--------------------------------------5.4.5 Calcul de la moyenne------------------------------------

def calcul_de_la_moyenne():
	score = [14, 9, 6, 8, 12]
	print("\nScore de l'eleve : ",score)
	sum = 0

	for i in range (len(score)):
		sum += score[i] 
	mean = sum / len(score)
	print("La moyenne de l'eleve est : ",mean)

#--------------------------------------5.4.6 Produit de nombres consécutifs------------------------------------

def produit():
	malist = list(range(0,21,2))
	print("\nListe entiers contenant les nombres entiers pairs de 2 a 20 inclus : ",malist)
	print("\nProduit des nombres consecutifs deux à deux de entiers en utilisant une boucle : \n")
	for i in range (0,len(malist)-1,1):
		produit = malist[i]*malist[i+1]
		print(produit)

#---------------------------------------5.4.7 Triangle-----------------------------------------------

def triangle():
	taille = input("\nInsert the size : ")
	while (not taille.isnumeric()):
		taille = input("\nInsert the size : ")
		if (not taille.isnumeric()):
			print("!!!!!!!!!Format error!!!!!!!!!!!!!")
		else:
			break
		
	size = int(taille)
	print(f"Triangle de taille {size} : ")
	for i in range(1,size+1,1):
	    print("*" * i)
    
#---------------------------------------5.4.8 Triangle inversé-------------------------------------

def triangle_Inverse():
	taille = input("\nInsert the size : ")
	while (not taille.isnumeric()):
		taille = input("\nInsert the size : ")
		if (not taille.isnumeric()):
			print("!!!!!!!!!Format error!!!!!!!!!!!!!")
		else:
			break
		
	size = int(taille)
	print(f"Triangle Inverse de taille {size} : ")
	for i in range (size, 0, -1):
	    print ("*" * i)
    
#--------------------------------------5.4.9 Triangle gauche--------------------------------------

def triangle_de_Gauche():
	taille = input("\nInsert the size : ")
	while (not taille.isnumeric()):
		taille = input("\nInsert the size : ")
		if (not taille.isnumeric()):
			print("!!!!!!!!!Format error!!!!!!!!!!!!!")
		else:
			break
		
	size = int(taille)
	
	print(f"Triangle de Gauche de taille {size} : ")
	for i in range(1,size+1):
		espace =' '*(size - i)
		ligne_triangle = espace + '*' * i
		print(ligne_triangle) 
    
#--------------------------------------5.4.10 Pyramide--------------------------------------

def pyramide():
	column = input("\nInsert the number column : ")
	if (not column.isnumeric()):
		while (not column.isnumeric()):
			column = input("Insert the number column (impair): ")
			if (not column.isnumeric()):
				print("!!!!!!!!!Format error!!!!!!!!!!!!!")
			else:
				column = int(column)
				while (column%2==0):
					column = int(input("Insert the number column (impair): "))
				break
	else:
		column = int(column)
		while (column%2==0):
			column = int(input("Insert the number column (impair): "))

	mid = int(column / 2)
	rows = int((column + 1)/2)
	k = 1
	print(f"Pyramide de taille {rows} : ")
	for i in range (1,rows+1,1):
		espace1 = ' '* (mid)
		value = "*" * k
		print(espace1 + value + espace1)
		mid=mid -1
		k = k+2

#------------------------------------5.4.11 Parcours de matrice----------------------------------------

def parcours_de_matrice():
	dimension = input("\nInsert the dimension : ")
	while (not dimension.isnumeric()):
		dimension = input("\nInsert the size : ")
		if (not dimension.isnumeric()):
			print("!!!!!!!!!Format error!!!!!!!!!!!!!")
		else:
			break
	dimension = int(dimension)

	maxiTab = [None] * dimension
	miniTab = [None] * dimension
	m= 1

	print(f"Parcours d'une matrice de taille {dimension} x {dimension} : ")

	for i in range (0, dimension,1):
		for j in range (0, dimension, 1):
			miniTab[j] = m
			m = m + 1
		maxiTab[i] = miniTab

	print("Ligne    colonne")
	for i in range (1,dimension+1,1):
		for j in range (1, dimension+1, 1):
			print (f"{i:>4d}{j:>8d}")
			
#--------------------------------5.4.12 Parcours de demi-matrice sans la diagonale (exercice ++)--------------------------------------------

def parcours_de_demi_matrice_sans_la_diagonale(): 
	dimension = input("\nInsert the dimension : ")
	while (not dimension.isnumeric()):
		dimension = input("\nInsert the size : ")
		if (not dimension.isnumeric()):
			print("!!!!!!!!!Format error!!!!!!!!!!!!!")
		else:
			break
	dimension = int(dimension)
	
	maxiTab = [None] * dimension
	miniTab = [None] * dimension
	m= 1
	l =0

	print(f"Parcours d'une matrice de taille {dimension} x {dimension} : ")

	for i in range (0, dimension,1):
		for j in range (0, dimension, 1):
			miniTab[j] = m
			m = m + 1
		maxiTab[i] = miniTab

	print("Ligne    colonne")
	for i in range (1,dimension+1,1):
		for j in range (i+1, dimension+1, 1):
			print (f"{i:>4d}{j:>8d}")
			l +=1
	print (f"Pour une matrice {dimension}x{dimension}, on a parcouru {l} case")

#---------------------------------5.4.13 Sauts de puce-------------------------------------------

def sauts_de_puce():
	import random
	number = 0
	compter = 0
	option = [-1,1]
	print("\nSauts de puces : ")
	while(number!=5):
		number = (number + random.choice(option))
		print(f"{number}_",end="")
		compter+=1
	print(f"\nLe nombres de sauts nécessaires pour réaliser ce parcours est : {compter}")

#--------------------------------5.4.14 Suite de Fibonacci (exercice +++)--------------------------------------------

def suite_Fibonacci():
	listFibo = [0,1]
	for i in range (2,16,1): 
		listFibo.append(listFibo[i-1] + listFibo[i-2]) 
	print("Suite Fibonacci avec rapport : [X0----->X15] ")
	print(f"\n{listFibo[0]}\n\n{listFibo[1]}\n")
	for n in range (2,16,1):
		rapport = listFibo[n] / listFibo[n-1]
		print(f"{listFibo[n]} : rapport = {rapport} \n")
 
 






