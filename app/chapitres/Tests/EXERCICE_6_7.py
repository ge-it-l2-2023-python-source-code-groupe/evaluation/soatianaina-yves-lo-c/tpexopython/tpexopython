#------------------------------------6.7.1 Jours de la semaine----------------------------------------

def jour():
    week =["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]

    print("\nweek = ",week)
    for i in range (0,7,1):
        if (i<4):
            print(f"Let work It's {week[i]}")
        if (i==4):
            print(f"Cool it's {week[i]}")
        if (i>4) :
            print(f"Take a break this week-end {week[i]}")

#------------------------------------6.7.2 Séquence complémentaire d’un brin d’ADN-----------------------------------------------------------------------

def brinComplementaire():
    brinCodant = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]
    print("\nBrin d'ADN :\n\nBrinCodant = ",brinCodant)
    nb = len(brinCodant)
    brinNonCodant = [None] * nb
    for i in range(0,nb,1):
        if (brinCodant[i]=="A"):
            brinNonCodant[i]="T"
        if (brinCodant[i]=="T"):
            brinNonCodant[i]="A"
        if (brinCodant[i]=="G"):
            brinNonCodant[i]="C"
        if (brinCodant[i]=="C"):
            brinNonCodant[i]="G"
    print("BrinNonCodant = ",brinNonCodant)

#------------------------------------6.7.3 Minimum d’une liste-------------------------------------------------------------------------------------------

def minList():
    list = [8, 4, 6, 1, 5]
    min = list[0]
    print ("\nLa liste des nombre est : ",list)
    for i in range (1,5,1):
        if (list[i]<min):
            min = list[i]
    print(f"Le min du list est {min}")

#------------------------------------6.7.4 Fréquence des acides aminés-----------------------------------------------------------------------------------

def frequenceD_AA():
    AA = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
    nb = len(AA)
    print("\nSequence d'acide amines : ",AA)
    freqA,freqG, freqW,freqR = 0,0,0,0
    for i in range (0,nb,1):
        if(AA[i]=="A"):
            freqA+= 1
        if(AA[i]=="G"):
            freqG+= 1
        if(AA[i]=="W"):
            freqW+= 1
        if(AA[i]=="R"):
            freqR+= 1
    print(f"La frequence des alanine (A)est : {freqA}\nLa frequence des arginine (R) est : {freqR}\nLa frequence des tryptophane (W) est : {freqW}\nLa frequence des glycines (G) est : {freqG}")
    
#------------------------------------6.7.5 Notes et mention d’un étudiant--------------------------------------------------------------------------------

def mention():
    note = [14,9,13,15,12]
    print ("\nLa liste des notes de l'etudiant : ",note)
    print(f"La note max est {max(note)}")
    print(f"La note max est {min(note)}")
    length = len(note)
    sum = 0

    for i in range (0,length,1):
        sum+= note[i]

    moyenne = sum / length
    print(f"La moyenne de l'eleve est : {moyenne:.2f}")

    if (10<=moyenne<12):
        print("Mention Passable")
    if (12<=moyenne<14):
        print("Mention Assez-Bien")
    if (14<=moyenne<16):
        print("Mention Bien")

#------------------------------------6.7.6 Nombres pairs-------------------------------------------------------------------------------------------------

def nbPair_Impairs():
    print("\nAffichage des nombres pairs inferieurs ou egaux a 10 d'une part, et les nombres impairs strictement superieurs a 10 d’autre part:\n")
    for i in range (0,21,1):
        if(i%2==0 and i<=10):
            print(i, end=" ")
        if(i%2==1 and i>10):
            print(i, end=" ")
    print("\n")

#------------------------------------6.7.7 Conjecture de Syracuse (exercice +++)-------------------------------------------------------------------------

def syracuse():
    nb = 98
    list = []
    nb1 = 1
    while nb1 < 60:
        if (nb%2==0):
            list.append(int (nb/2))
            nb = int (nb/2)
        else:
            list.append(nb*3 +1)
            nb = nb*3+1
        if nb==1:
            trivial=list[-3:]
        nb1+=1

    print("\nLa list des nombres de la suite de Syracuse : ",list)   
    print("Les nombres qui constituent le cycle trivial sont : ",trivial,"\n")

#------------------------------------6.7.8 Attribution de la structure secondaire des acides aminés d’une protéine---------------------------------------

def structureSecondaire():
    ifte = [[48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
            [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
    [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
    [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
    [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]]

    print("\nListe ITFE : ",ifte)
    print("\nAttribution de la structure secondaire des acides aminés d’une protéine : ")
    for acide_amine in ifte:
        if -57 - 30 < acide_amine[0] < -57 + 30 \
            and -47 - 30 < acide_amine[0] < -47 + 30:
            print(f"L'acide-amine {acide_amine} est en hélice")
        else:
            print(f"L'acide-amine {acide_amine} n'est pas en hélice")
    print("\nD'apres-moi la structure secondaire majoritaire de ces 15 acides amines est en helices") 

#------------------------------------6.7.9 Détermination des nombres premiers inférieurs à 100-----------------------------------------------------------

def nbPremiers():
    #first method
    print("\nDétermination des nombres premiers inférieurs à 100 :\n"
        "\n1-Premier methode (peu optimale mais assez intuitive) : ")
    compteur = 0
    for i in range (2,101,1):
        for j in range (1,i+1,1):
            if i%j == 0 :
                compteur += 1
        if compteur == 2: 
            print(i, end=" ")
        compteur = 0 
    print("\n")
    #second method
    premier = [2]
    print("2-Deuxieme methodes (plus optimale et plus rapide, mais un peu plus compliquée) : ")
    for i in range (4,101,1):
        index = 0
        for j in range (0,len(premier),1):
            if i%premier[j] != 0:
                index +=1
        if(index==len(premier)):
            premier.append(i)
    print(premier,"\n")

#------------------------------------6.7.10 Recherche d’un nombre par dichotomie-------------------------------------------------------------------------

def dichotomie():
    print ("\nRechercher par dichotomie\n"
        "\nTry to fix a number between 1 and 100 silently, I'll guess it\n")
    intervalle = [0,100]
    choice ="+"
    compter = 0
    while (choice != "=") :
        mid = int((intervalle[0] + intervalle[1]) / 2)
        choice = input(f"The number is lower or greatest than {mid} ? [+/-/=] ")
        while(choice!= "+" and choice!="-" and choice!="="):
           choice = input(f"!!!The number is lower or greatest than {mid} ? [+/-/=] ") 
        if choice == "+":
            intervalle[0] = mid
        elif choice == "-":
            intervalle[1] = mid
        else :
            print(f"the number is {mid}")
            break
        compter+=1
    print(f"Yeah!!! I've guess the number in {compter+1} attempt")