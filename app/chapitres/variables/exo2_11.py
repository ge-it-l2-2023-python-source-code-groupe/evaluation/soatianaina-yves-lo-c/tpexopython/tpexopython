#-----------------------------------2.11.1 Nombres de Friedman--------------------------------------------------------

def friedman():
      print("\nTest d'expression mathématique:"
            f"\nL'expression 7 + 3\u2076 = {7+3**6} --> est un nombre de Friedman"
            f"\nL'expression (3+4)³ = {(3+4)**3} --> est un nombre de Friedman"
            f"\nL'expression 3\u2076 - 5 = {(3**6) - 5} --> n'est pas un nombre de Friedman"
            f"\nL'expression (1 + 2\u2078) x 5 = {(1+2**8) * 5} --> est un nombre de Friedman"
            f"\nL'expression (2 + 1\u2078)\u2077 = {(2+1**8)**7} --> est un nombre de Friedman"
      )

#------------------------------------2.11.2 Prédire le résultat : opérations---------------------------------------------

def prediction_1():
      print("\nPrediction des resultats :"
             f"\n(1+2)**3  -----> {(1+2)**3}"
             f"\n'Da' * 4  -----> {'Da' * 4}"
             f"\nDa + 3  -----> TypeError: can only concatenate str (not int) to str"
             f"\n('Pa' + 'Pa') * 4  -----> {('Pa'+'La') * 2}"
             f"\n('Da' * 4) / 2  -----> TypeError: unsupported operand type(s) for /: 'str' and 'int'"
             f"\n5/2 -----> {5/2}"
             f"\n5//2 ------> {5//2}"
             f"\n5%2 ------> {5%2}"
            )
      
#---------------------------------- 2.11.3 Prédire le résultat : opérations et conversions de types------------------------

def prediction_2():
      print("\nPrediction des resultats :"
            f"\nstr(4) * int('3') -----> {str(4) * int('3')}"
            f"\nint('3') + float('3.2') -----> {int('3') + float('3.2')}"
            "\nstr(3) * float('3.2')  --------> TypeError: can't multiply sequence by non-int of type 'float'"
            f"\nstr(3/4) * 2  -----------> {str(3/4) * 2}")

