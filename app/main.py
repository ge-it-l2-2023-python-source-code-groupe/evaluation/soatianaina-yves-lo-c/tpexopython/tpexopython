from chapitres.variables.exo2_11 import *
from chapitres.affichage.EXERCICE_3_6 import *
from chapitres.liste__range.Exo4_10 import *
from chapitres.Boucles_et_Comparaisons.Exo5_4 import *
from chapitres.Tests.EXERCICE_6_7 import *
from chapitres.waitForKeyboard import *
from tools.console import clear
from tools.console import clear


selection = "0"
choix = "0"
while selection != "6" and selection !="exit":
    clear()
    print("✨✨✨✨✨BIENVENU✨✨✨✨✨")
    print("\n\nChapitres : \n\n"
          "1-Variable"
          "\n2-Affichage"
          "\n3-Liste"
          "\n4-Boucles et comparaisons"
          "\n5-Tests"
          "\n6-exit")
    selection = input("\n👆 Choisissez une option 😊: ")
    
    if selection == "6" or selection =="exit":
        print("🎄🎄🎅Bye et joyeux noël🎅🎄🎄")

    #Pour le chapitre VARIABLE

    while selection == "1":
        clear()
        print("💫💫💫💫Chapitre {VARIABLE}💫💫💫💫 "
              "\n\n1-Nombres de Friedman"
              "\n2-Opérations"
              "\n3-Opérations et conversions de types"
              "\n4-exit")
        choix = input("\n👆 Voir exercice N : ")
        if choix == "1":
            # appeller la methode  friedman() du code du fichier exo2_11_1.py ici
            friedman()
        elif choix == "2":
            prediction_1()
        elif choix == "3":
            prediction_1()
        elif choix == "4" or choix =="exit":
            clear()
            break
        wait()
        clear()

    #Pour le chapitre AFFICHAGE

    while (selection == "2" and choix!=6):
        clear()
        print("💦💦💦💦 Chapitre {AFFICHAGE} 💦💦💦💦"
              "\n\n1-Affichage dans l’interpréteur et dans un programme"
              "\n2-Poly-A"
              "\n3-Poly-A et poly-GC"
              "\n4-Écriture formatée"
              "\n5-Écriture formatée 2"
              "\n6-exit")
        choix = input("\n👆 Voir exercice N : ")
        if choix == "1":
            affichage()
        elif choix == "2":
            poly_A()
        elif choix == "3":
            poly_A_G()
        elif choix == "4":
           ecriture_Formate()
        elif choix == "5":
           ecriture_Formate2()
        elif choix == "6" or choix =="exit":
           clear()
           break
        wait()

    #Pour le chapitre LIST && RANGE && INDICE

    while selection == "3":
        clear()
        print("💢💢💢💢 Chapitre {LISTE} 💢💢💢💢"
              "\n\n1-Jours de la semaine"
              "\n2-Saisons"
              "\n3-Table de multiplication par 9"
              "\n4-Nombres pairs"
              "\n5-List avec indice"
              "\n6-List avec range"
              "\n7-Exit")
        choix = input("\n👆 Voir exercice N : ")
        if choix == "1":
            week()
        elif choix == "2":
            saisons()
        elif choix == "3":
            multiplication()
        elif choix == "4":
           nbPaire()     
        elif choix == "5":
           listeEtIndice()
        elif choix == "6":
            listeEtRange()
        elif choix == "7" or choix =="exit":
            clear()
            break
        wait()

    #Pour le chapitre BOUCLE && COMPARAISONS

    while selection == "4":
        clear()
        print("💥💥💥💥 Chapitre {BOUCLES_ET_COMPARAISONS} 💥💥💥💥"
              "\n\n1 Boucles de base"
              "\n2 Boucle et jours de la semaine"
              "\n3 Nombres de 1 à 10 sur une ligne"
              "\n4 Nombres pairs et impairs"
              "\n5 Calcul de la moyenne"
              "\n6 Produit de nombres consécutifs"
              "\n7 Triangle"
              "\n8 Triangle inversé"
              "\n9 Triangle gauche"
              "\n10 Pyramide"
              "\n11 Parcours de matrice"
              "\n12 Parcours de demi-matrice sans la diagonale (exercice ++)"
              "\n13 Sauts de puce"
              "\n14 Suite de Fibonacci (exercice +++)"
              "\n15 exit")
        choix = input("\n👆 Voir exercice N : ")
        if choix == "1":
            boucles_de_base()
        elif choix == "2":
            boucles_et_jours_de_la_semaine()
        elif choix == "3":
            nb_de1_10_sur_une_ligne()
        elif choix == "4":
           nb_Pairs_et_Impairs()
        elif choix == "5":
            calcul_de_la_moyenne()
        elif choix == "6":
            produit()
        elif choix == "7":
            triangle()
        elif choix == "8":
            triangle_Inverse()
        elif choix == "9":
            triangle_de_Gauche()
        elif choix == "10":
            pyramide()
        elif choix == "11":
            parcours_de_matrice()
        elif choix == "12":
            parcours_de_demi_matrice_sans_la_diagonale()
        elif choix == "13":
            sauts_de_puce()
        elif choix == "14":
            suite_Fibonacci()
        elif choix == "15" or choix =="exit":
            clear()
            break
        wait()

    #Pour le chapitre TESTS

    while selection == "5":
        clear()
        print("💎💎💎💎 Chapitre {TESTS} 💎💎💎💎"
              "\n\n1-Jours de la semaine"
              "\n2-Séquence complémentaire d’un brin d’ADN"
              "\n3-Minimum d’une liste"
              "\n4-Fréquence des acides aminés"
              "\n5-Notes et mention d’un étudiant"
              "\n6-Nombres pairs"
              "\n7-Conjecture de Syracuse"
              "\n8-Attribution de la structure secondaire des acides aminés d’une protéine"
              "\n9-Détermination des nombres premiers inférieurs à 100"
              "\n10-Recherche d’un nombre par dichotomie"
              "\n11-exit")
        choix = input("\n👆Voir exercice N : ")
        if choix == "1":
            jour()
        elif choix == "2":
            brinComplementaire()
        elif choix == "3":
            minList()
        elif choix == "4":
           frequenceD_AA()    
        elif choix == "5":
           mention()
        elif choix == "6":
            nb_Pairs_et_Impairs()
        elif choix == "7":
            syracuse()
        elif choix == "8":
            structureSecondaire()
        elif choix == "9":
            nbPremiers()
        elif choix == "10":
            dichotomie()
        elif choix == "11" or choix =="exit":
            break
        wait()
    wait()
    clear()
    

   
